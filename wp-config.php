<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mmjmenu' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'kw?l`1} ?/>R!A2%Qh[,66VvbX-7^:?S-,znNYFoBTLn&5$01F;~nG|[4UW`g53B' );
define( 'SECURE_AUTH_KEY',  '5@U/zN#*(k=TigML[WWK6Pn yv=F_xI3 **{wd=k=2%?2I(b~p;kcE=4R1|`kLyp' );
define( 'LOGGED_IN_KEY',    'Z+K~VKIt9t[1q2.tf)f=|3Ga6Kk9hMoM-2UzFar_X0xG;^|+4jz64_%i4-}.[z3n' );
define( 'NONCE_KEY',        '.qP^%=q$hG*LzjLhdUPW(z IV8:(d$r1#.+gt^_1d+AGv4#ALf lN@7cBu_#;-J:' );
define( 'AUTH_SALT',        'K#9Y-U%-@@TOdAml[.=b+2Pk]M@i=|fH7<m7bOk!#H.F+&1R{V/unmF%Il:xH)%)' );
define( 'SECURE_AUTH_SALT', 'Lg+tT<N[R8^Mx-WPyw^FXF@1[`zi$bf]O=GKn7R$xCz<Uo>$]w*X<m NxlDyr?eH' );
define( 'LOGGED_IN_SALT',   'vkwO8rd ]iNu=N*abo7m}:&Q>QJan{/Ior67M$TRd|.(5(Z_!$UIpyej.EX_$}sM' );
define( 'NONCE_SALT',       '860[^k`^$^l<sDG[t[Vsga44)etC%d*/53Ev!~$gSLKdN>J7qUHv8ADF2uvcoVJx' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
