<?php
/**
 * Twenty Nineteen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

/**
 * Twenty Nineteen only works in WordPress 4.7 or later.
 */
// global $client;
// require 'Mmjmenu.php';
// $client = new Mmjmenu('LRmwOMqXISzPJZnLpxRQDDbZj');


if ( version_compare( $GLOBALS['wp_version'], '4.7', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

if ( ! function_exists( 'twentynineteen_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function twentynineteen_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Twenty Nineteen, use a find and replace
		 * to change 'twentynineteen' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'twentynineteen', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'menu-1' => __( 'Primary', 'twentynineteen' ),
				'footer' => __( 'Footer Menu', 'twentynineteen' ),
				'social' => __( 'Social Links Menu', 'twentynineteen' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 190,
				'width'       => 190,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style-editor.css' );

		// Add custom editor font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => __( 'Small', 'twentynineteen' ),
					'shortName' => __( 'S', 'twentynineteen' ),
					'size'      => 19.5,
					'slug'      => 'small',
				),
				array(
					'name'      => __( 'Normal', 'twentynineteen' ),
					'shortName' => __( 'M', 'twentynineteen' ),
					'size'      => 22,
					'slug'      => 'normal',
				),
				array(
					'name'      => __( 'Large', 'twentynineteen' ),
					'shortName' => __( 'L', 'twentynineteen' ),
					'size'      => 36.5,
					'slug'      => 'large',
				),
				array(
					'name'      => __( 'Huge', 'twentynineteen' ),
					'shortName' => __( 'XL', 'twentynineteen' ),
					'size'      => 49.5,
					'slug'      => 'huge',
				),
			)
		);

		// Editor color palette.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Primary', 'twentynineteen' ),
					'slug'  => 'primary',
					'color' => twentynineteen_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 33 ),
				),
				array(
					'name'  => __( 'Secondary', 'twentynineteen' ),
					'slug'  => 'secondary',
					'color' => twentynineteen_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 23 ),
				),
				array(
					'name'  => __( 'Dark Gray', 'twentynineteen' ),
					'slug'  => 'dark-gray',
					'color' => '#111',
				),
				array(
					'name'  => __( 'Light Gray', 'twentynineteen' ),
					'slug'  => 'light-gray',
					'color' => '#767676',
				),
				array(
					'name'  => __( 'White', 'twentynineteen' ),
					'slug'  => 'white',
					'color' => '#FFF',
				),
			)
		);

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );
	}
endif;
add_action( 'after_setup_theme', 'twentynineteen_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twentynineteen_widgets_init() {

	register_sidebar(
		array(
			'name'          => __( 'Footer', 'twentynineteen' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your footer.', 'twentynineteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

}
add_action( 'widgets_init', 'twentynineteen_widgets_init' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width Content width.
 */
function twentynineteen_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'twentynineteen_content_width', 640 );
}
add_action( 'after_setup_theme', 'twentynineteen_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function twentynineteen_scripts() {
	wp_enqueue_style( 'twentynineteen-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );

	wp_style_add_data( 'twentynineteen-style', 'rtl', 'replace' );

	if ( has_nav_menu( 'menu-1' ) ) {
		wp_enqueue_script( 'twentynineteen-priority-menu', get_theme_file_uri( '/js/priority-menu.js' ), array(), '1.1', true );
		wp_enqueue_script( 'twentynineteen-touch-navigation', get_theme_file_uri( '/js/touch-keyboard-navigation.js' ), array(), '1.1', true );
	}

	wp_enqueue_style( 'twentynineteen-print-style', get_template_directory_uri() . '/print.css', array(), wp_get_theme()->get( 'Version' ), 'print' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'twentynineteen_scripts' );

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function twentynineteen_skip_link_focus_fix() {
	// The following is minified via `terser --compress --mangle -- js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'twentynineteen_skip_link_focus_fix' );

/**
 * Enqueue supplemental block editor styles.
 */
function twentynineteen_editor_customizer_styles() {

	wp_enqueue_style( 'twentynineteen-editor-customizer-styles', get_theme_file_uri( '/style-editor-customizer.css' ), false, '1.1', 'all' );

	if ( 'custom' === get_theme_mod( 'primary_color' ) ) {
		// Include color patterns.
		require_once get_parent_theme_file_path( '/inc/color-patterns.php' );
		wp_add_inline_style( 'twentynineteen-editor-customizer-styles', twentynineteen_custom_colors_css() );
	}
}
add_action( 'enqueue_block_editor_assets', 'twentynineteen_editor_customizer_styles' );

/**
 * Display custom color CSS in customizer and on frontend.
 */
function twentynineteen_colors_css_wrap() {

	// Only include custom colors in customizer or frontend.
	if ( ( ! is_customize_preview() && 'default' === get_theme_mod( 'primary_color', 'default' ) ) || is_admin() ) {
		return;
	}

	require_once get_parent_theme_file_path( '/inc/color-patterns.php' );

	$primary_color = 199;
	if ( 'default' !== get_theme_mod( 'primary_color', 'default' ) ) {
		$primary_color = get_theme_mod( 'primary_color_hue', 199 );
	}
	?>

	<style type="text/css" id="custom-theme-colors" <?php echo is_customize_preview() ? 'data-hue="' . absint( $primary_color ) . '"' : ''; ?>>
		<?php echo twentynineteen_custom_colors_css(); ?>
	</style>
	<?php
}
add_action( 'wp_head', 'twentynineteen_colors_css_wrap' );

function wpdocs_theme_name_scripts() {
    wp_enqueue_script( 'script-name', get_template_directory_uri() . '/js/script.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

function get_data() {

	$url = site_url();
//	wp_die(get_data_for_lookup_table( $id, $table ));

    require 'Mmjmenu.php';
	$client = new Mmjmenu('LRmwOMqXISzPJZnLpxRQDDbZj');

	$menuItems      = $client->menuItems();
	$menuItems      = json_decode($menuItems, true);

	$num=0;
	$num1=1;
	$cat=array();
    foreach($menuItems['menu_items'] as $item){
	if($num<5){
		if(post_exists($item['name'],'','','')){
			$mypost = get_page_by_title($item['name'],'','product');
		
	 $meta=get_post_meta($mypost->ID, 'product_updated_at', true);
	 if($meta != $item['updated_at']){
		$price=5;
		  $post = array(
			'post_type'    => "product",
			'post_title'   =>  $item['name'],
			'post_content' =>  wp_strip_all_tags($item['body_html']),
			'post_status'  =>  'publish'
		);
		$post_id=wp_insert_post( $post, $wp_error );

		// $asgn_cat = wc_get_product($post_id);
		// $asgn_cat->set_category_ids([ 300, 400 ] );


		// Set Product Image 
	     	require_once(ABSPATH . 'wp-admin/includes/file.php');
			require_once(ABSPATH . 'wp-admin/includes/media.php');
			$thumb_url="https://wm-mmjmenu-images-production.s3.amazonaws.com/menu_items/images/2959789/square/CBD water.jpg?1568656898";
		//	var_dump($thumb_url);
			$tmp = download_url( $thumb_url );

			preg_match('/[^\?]+\.(jpg|JPG|jpe|JPE|jpeg|JPEG|gif|GIF|png|PNG)/', $thumb_url, $matches);
			$file_array['name'] = basename($matches[0]);
			$file_array['tmp_name'] = $tmp;

			if ( is_wp_error( $tmp ) ) {
			@unlink($file_array['tmp_name']);
			$file_array['tmp_name'] = '';
			$logtxt .= "Error: download_url error – $tmp\n";
			}else{
			$logtxt .= 'download_url: $tmp\n';
			}

			$thumbid = media_handle_sideload( $file_array, $post_id, 'gallery desc' );
			if ( is_wp_error($thumbid) ) {
			@unlink($file_array['tmp_name']);

			$logtxt .= 'Error: media_handle_sideload error – $thumbid\n';
			}else{
			$logtxt .= 'ThumbID: $thumbid\n';
			}
			set_post_thumbnail($post_id, $thumbid);
		// Set Product Image 
	
			$api_category=$item['category'];
			$term_ids=[25,26,27,28,29,30,31,32,33,34,35,36];
			foreach($term_ids as $term){
			$term_obj = get_term( $term ,'product_cat' );
				$term_name = $term_obj->name;
				if($api_category == $term_name){
					wp_set_object_terms( $post_id, $term, 'product_cat' );		
				}
			}
		// Get Product Categories
		update_post_meta($post_id, '_stock_status', 'instock');
		$product = new WC_Product_Variable( $post_id );
		$product->save();

		$post_except=['Weight:gram','Weight:eighth','Weight:quarter','Weight:half','Weight:ounce','Weight:half_gram','Weight:two_gram'];
		$product = wc_get_product( $post_id );
		$varient_name=['gram','eighth','quarter','half','ounce','half_gram','two_gram'];
		$vn=0;
		foreach($post_except as $item1) {
		 $var= $post_id+$num1;
		  $variation_post = array(
			  'post_title'   => $product->get_title() .'-'. $varient_name[$vn],
			  'post_name'   => $varient_name[$vn],
			//  'post_excerpt' => $item1,
			  'post_status'  => 'publish',
			  'post_parent'  => $post_id,
			  'post_type'    => 'product_variation',
		       'guid'        =>  $url.'/?post_type=product_variation&p='.$var
		  );
	
		  $variation_id = wp_insert_post( $variation_post );
		  $variation = new WC_Product_Variation( $variation_id );		
		  $variation->set_name($item1);
          $variation->set_regular_price(strval($item['price'][$varient_name[$vn]]));
		  $variation->set_stock_quantity( '10' );
		  $variation->set_description($varient_name[$vn]);
		  $variation->set_manage_stock(true);
		  $variation->set_stock_status('instock');
		  $variation->save();
		 $price++;
		 $vn++;
		  $num1++;
	   }	

	$variation_data =
		array(
			'weight' => 
			array(
				'name' => 'weight',
				'value' => 'gram | eighth | quarter | half | ounce | half_gram | two_gram',
				'position'=>'',
				'is_visible'=>1,
				'is_variation'=>1,
				'is_taxonomy'=>0,
			),
	    );

update_post_meta($post_id, '_product_attributes', $variation_data);

	  if($post_id){
		add_post_meta($post_id, 'product_mmj_id',$item['id']);
		add_post_meta($post_id, 'product_measurement',$item['measurement']);
		add_post_meta($post_id, 'product_updated_at',$item['updated_at']);
	  }
	//    $variation_data = serialize( $variation_data );
	//    update_post_meta( $variation_id, '_product_attributes',$variation_data);
	  
	
	   $num++;
	}else{
		echo "Products already updated";
	}

	}else{
		$price=5;
		$post = array(
		  'post_type'    => "product",
		  'post_title'   =>  $item['name'],
		  'post_content' =>  wp_strip_all_tags($item['body_html']),
		  'post_status'  =>  'publish'
	  );
	  $post_id=wp_insert_post( $post, $wp_error );

	  // $asgn_cat = wc_get_product($post_id);
	  // $asgn_cat->set_category_ids([ 300, 400 ] );


	  // Set Product Image 
		   require_once(ABSPATH . 'wp-admin/includes/file.php');
		  require_once(ABSPATH . 'wp-admin/includes/media.php');
		  $thumb_url="https://wm-mmjmenu-images-production.s3.amazonaws.com/menu_items/images/2959789/square/CBD water.jpg?1568656898";
	  //	var_dump($thumb_url);
		  $tmp = download_url( $thumb_url );

		  preg_match('/[^\?]+\.(jpg|JPG|jpe|JPE|jpeg|JPEG|gif|GIF|png|PNG)/', $thumb_url, $matches);
		  $file_array['name'] = basename($matches[0]);
		  $file_array['tmp_name'] = $tmp;

		  if ( is_wp_error( $tmp ) ) {
		  @unlink($file_array['tmp_name']);
		  $file_array['tmp_name'] = '';
		  $logtxt .= "Error: download_url error – $tmp\n";
		  }else{
		  $logtxt .= 'download_url: $tmp\n';
		  }

		  $thumbid = media_handle_sideload( $file_array, $post_id, 'gallery desc' );
		  if ( is_wp_error($thumbid) ) {
		  @unlink($file_array['tmp_name']);

		  $logtxt .= 'Error: media_handle_sideload error – $thumbid\n';
		  }else{
		  $logtxt .= 'ThumbID: $thumbid\n';
		  }
		  set_post_thumbnail($post_id, $thumbid);
	  // Set Product Image 
  
		  $api_category=$item['category'];
		  $term_ids=[25,26,27,28,29,30,31,32,33,34,35,36];
		  foreach($term_ids as $term){
		  $term_obj = get_term( $term ,'product_cat' );
			  $term_name = $term_obj->name;
			  if($api_category == $term_name){
				  wp_set_object_terms( $post_id, $term, 'product_cat' );		
			  }
		  }
	  // Get Product Categories
	  update_post_meta($post_id, '_stock_status', 'instock');
	  $product = new WC_Product_Variable( $post_id );
	  $product->save();

	  $post_except=['Weight:gram','Weight:eighth','Weight:quarter','Weight:half','Weight:ounce','Weight:half_gram','Weight:two_gram'];
	  $product = wc_get_product( $post_id );
	  $varient_name=['gram','eighth','quarter','half','ounce','half_gram','two_gram'];
	  $vn=0;
	  foreach($post_except as $item1) {
	   $var= $post_id+$num1;
		$variation_post = array(
			'post_title'   => $product->get_title() .'-'. $varient_name[$vn],
			'post_name'   => $varient_name[$vn],
		  //  'post_excerpt' => $item1,
			'post_status'  => 'publish',
			'post_parent'  => $post_id,
			'post_type'    => 'product_variation',
			 'guid'        =>  $url.'/?post_type=product_variation&p='.$var
		);
  
		$variation_id = wp_insert_post( $variation_post );
		$variation = new WC_Product_Variation( $variation_id );		
		$variation->set_name($item1);
		$variation->set_regular_price(strval($item['price'][$varient_name[$vn]]));
		$variation->set_stock_quantity( '10' );
		$variation->set_description($varient_name[$vn]);
		$variation->set_manage_stock(true);
		$variation->set_stock_status('instock');
		$variation->save();
	   $price++;
	   $vn++;
		$num1++;
	 }	

  $variation_data =
	  array(
		  'weight' => 
		  array(
			  'name' => 'weight',
			  'value' => 'gram | eighth | quarter | half | ounce | half_gram | two_gram',
			  'position'=>'',
			  'is_visible'=>1,
			  'is_variation'=>1,
			  'is_taxonomy'=>0,
		  ),
	  );

update_post_meta($post_id, '_product_attributes', $variation_data);

	if($post_id){
	  add_post_meta($post_id, 'product_mmj_id',$item['id']);
	  add_post_meta($post_id, 'product_measurement',$item['measurement']);
	  add_post_meta($post_id, 'product_updated_at',$item['updated_at']);
	}
  //    $variation_data = serialize( $variation_data );
  //    update_post_meta( $variation_id, '_product_attributes',$variation_data);
	
  
	 $num++;

	 }
	
	}

	}

	wp_die('Success');  
}
add_action( 'wp_ajax_nopriv_get_data', 'get_data' );
add_action( 'wp_ajax_get_data', 'get_data' );


/**
 * SVG Icons class.
 */
require get_template_directory() . '/classes/class-twentynineteen-svg-icons.php';

/**
 * Custom Comment Walker template.
 */
require get_template_directory() . '/classes/class-twentynineteen-walker-comment.php';

/**
 * Enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * SVG Icons related functions.
 */
require get_template_directory() . '/inc/icon-functions.php';

/**
 * Custom template tags for the theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


	//   $cat[]= $item['category'];
	//   $cat=array_unique($cat);

	  		// Get Product Categories	
			//   $taxonomy     = 'product_cat';
			//   $orderby      = 'name';  
			//   $show_count   = 0;      // 1 for yes, 0 for no
			//   $pad_counts   = 0;      // 1 for yes, 0 for no
			//   $hierarchical = 1;      // 1 for yes, 0 for no  
			//   $title        = '';  
			//   $empty        = 0;
  
			//   $cat_details = array(
			// 	  'taxonomy'     => $taxonomy,
			// 	  'orderby'      => $orderby,
			// 	  'show_count'   => $show_count,
			// 	  'pad_counts'   => $pad_counts,
			// 	  'hierarchical' => $hierarchical,
			// 	  'title_li'     => $title,
			// 	  'hide_empty'   => $empty
			//   );
			//   $cat_arr=array();
			//   $all_categories = get_categories( $cat_details );
			//   foreach ($all_categories as $cat) {
			// 	  $cat_arr[]= $cat->name;
			//   }



			// $menuItem      = $client->menuItem('2591401');
    // $menuItem      = json_decode($menuItem, true);
	// $menuItem      = $menuItem['menu_item'];
	// // wp_die($menuItem['name']);
	// $id =get_current_user_id();


	
	// //Create post


	// if($post_id){
	// 	$attach_id = get_post_meta($product->parent_id, "_thumbnail_id", true);
	// 	add_post_meta($post_id, '_thumbnail_id', $attach_id);
	// }
	
	// wp_set_object_terms( $post_id, 'Races', 'product_cat' );
	// wp_set_object_terms($post_id, 'simple', 'product_type');
	
	// update_post_meta( $post_id, '_visibility', 'visible' );
	// update_post_meta( $post_id, '_stock_status', 'instock');
	// update_post_meta( $post_id, 'total_sales', '0');
	// update_post_meta( $post_id, '_downloadable', 'yes');
	// update_post_meta( $post_id, '_virtual', 'yes');
	// update_post_meta( $post_id, '_regular_price', "1" );
	// update_post_meta( $post_id, '_sale_price', "1" );
	// update_post_meta( $post_id, '_purchase_note', "" );
	// update_post_meta( $post_id, '_featured', "no" );
	// update_post_meta( $post_id, '_weight', "" );
	// update_post_meta( $post_id, '_length', "" );
	// update_post_meta( $post_id, '_width', "" );
	// update_post_meta( $post_id, '_height', "" );
	// update_post_meta($post_id, '_sku', "");
	// update_post_meta( $post_id, '_product_attributes', array());
	// update_post_meta( $post_id, '_sale_price_dates_from', "" );
	// update_post_meta( $post_id, '_sale_price_dates_to', "" );
	// update_post_meta( $post_id, '_price', "1" );
	// update_post_meta( $post_id, '_sold_individually', "" );
	// update_post_meta( $post_id, '_manage_stock', "no" );
	// update_post_meta( $post_id, '_backorders', "no" );
	// update_post_meta( $post_id, '_stock', "" );
	
	// // file paths will be stored in an array keyed off md5(file path)
	// $downdloadArray =array('name'=>"Test", 'file' => $uploadDIR['baseurl']."/video/".$video);
	
	// $file_path =md5($uploadDIR['baseurl']."/video/".$video);
	
	
	// $_file_paths[  $file_path  ] = $downdloadArray;
	// // grant permission to any newly added files on any existing orders for this product
	// // do_action( 'woocommerce_process_product_file_download_paths', $post_id, 0, $downdloadArray );
	// update_post_meta( $post_id, '_downloadable_files', $_file_paths);
	// update_post_meta( $post_id, '_download_limit', '');
	// update_post_meta( $post_id, '_download_expiry', '');
	// update_post_meta( $post_id, '_download_type', '');
	// update_post_meta( $post_id, '_product_image_gallery', '');

	// https://wordpress.stackexchange.com/questions/258077/how-to-assign-specific-attribute-to-variation-for-woocommerce-product
