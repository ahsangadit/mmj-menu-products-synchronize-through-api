<?php
/*
Template Name: My Template
*/
get_header();

// require 'Mmjmenu.php';
// $client = new Mmjmenu('LRmwOMqXISzPJZnLpxRQDDbZj');

// $menuItems      = $client->menuItems();
// $menuItems      = json_decode($menuItems, true);

// foreach($menuItems['menu_items'] as $item){
// 	echo $item['id']."<br>";
// }

// $menuItem      = $client->menuItem('2591401');
// $menuItem      = json_decode($menuItem, true);
// $menuItem      = $menuItem['menu_item'];

// print($menuItem['name']);
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
				the_post();
				get_template_part( 'template-parts/content/content' );
			}

			// Previous/next page navigation.
			twentynineteen_the_posts_navigation();

		} else {

			// If no content, include the "No posts found" template.
			get_template_part( 'template-parts/content/content', 'none' );

		}
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php
get_footer();
